import axios from "axios/index";

class DataComponent {
  constructor () {
    this.searchUrl = "https://www.food2fork.com/api/search";
    this.detailsUrl = "https://www.food2fork.com/api/get";
    this.formData = new FormData();
    this.formData.append("key", "2569fda07d15719f87c170789c4ff967");
    this.config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    };
  }

  async findAllBySearchQuery (searchString) {
    try {
      this.formData.set("q", searchString);
      const response = await axios.post(this.searchUrl, this.formData,
        this.config);

      return response.data;
    } catch (err) {
      console.log(err);
    }
  }

  async findOneById (recipeId) {
    try {
      this.formData.set("rId", recipeId);
      const response = await axios.post(this.detailsUrl, this.formData,
        this.config);

      return response.data;
    } catch (err) {
      console.log(err);
    }
  }
}

export default DataComponent;