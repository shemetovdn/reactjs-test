import React from "react";
import {
  HashRouter,
  Route,
  Switch
} from "react-router-dom";
import RecipesList from "./../recipes/recipes-list.jsx";
import RecipeItem from "./../recipes/recipe-item.jsx";

class Router extends React.Component {
  constructor (props) {
    super(props);
    this.state = { isSuperAdmin: window.isSuperAdmin };
  }

  render () {
    return (
      <HashRouter>
        <Switch>
          <Route path="/recipe/:id" component={RecipeItem}/>
          <Route path="/" component={RecipesList}/>
        </Switch>
      </HashRouter>
    );
  }
}

export default Router;