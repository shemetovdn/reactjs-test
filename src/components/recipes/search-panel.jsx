import React from "react";
import DataComponent from "../common/data-component.js";

class SearchPanel extends React.Component {
  constructor (props) {
    super(props);
    this.state = { searchString: "" };
    this.dataComponent = new DataComponent();
  }

  async componentDidMount () {
    const result = await this.dataComponent.findAllBySearchQuery(
      this.state.searchString);
    this.props.setRecipes(result.recipes, result.count);
  }

  searchFieldChange = (event) => {
    this.setState({ searchString: event.target.value });
  }

  searching = async (event) => {
    event.preventDefault();
    const result = await this.dataComponent.findAllBySearchQuery(
      this.state.searchString);
    this.props.setRecipes(result.recipes, result.count);
  }

  render () {
    return (

      <div className={"container"}>
        <div className={"form-group"}>
          <label>Search</label>
          <input type={"text"} className={"form-control"}
            value={this.state.searchString}
            onChange={this.searchFieldChange}
          />
          <div className={"row pull-right"}>
            <button onClick={this.searching}
              className={"btn btn-primary"}>Search
            </button>
          </div>
        </div>
      </div>

    );
  }
}

export default SearchPanel;