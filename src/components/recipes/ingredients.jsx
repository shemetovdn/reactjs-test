import React from "react";
import LoaderImage from "../../img/basicloader.gif";

class Ingredients extends React.Component {
  constructor (props) {
    super(props);
  }

  renderIngredients = () => {
    if (this.props.ingredients instanceof Object) {
      return this.props.ingredients.map((item, i) => (<li key={i}>{item}</li>));
    } else {
      return <img className={"loader"} src={LoaderImage}/>;
    }
  }

  render () {
    return (
      <ul>
        {this.renderIngredients()}
      </ul>
    );
  }
}

export default Ingredients;