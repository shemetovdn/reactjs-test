import React from "react";
import DataComponent from "../common/data-component.js";
import Ingredients from "./ingredients.jsx";

class RecipeItem extends React.Component {
  constructor (props) {
    super(props);
    this.state = { recipe: { recipe_id: props.match.params.id } };
    this.dataComponent = new DataComponent();
  }

  async componentDidMount () {
    const result = await this.dataComponent.findOneById(
      this.state.recipe.recipe_id);
    this.setState({
      recipe: result.recipe
    });
  }

  backHome = () => {
    this.props.history.push("/");
  }

  render () {
    return (
      <>
        <button onClick={this.backHome}>Back</button>
        <h1>{this.state.recipe.title}</h1>
        <div className={"row recipe"}>
          <div className={"col-md-4"}><img src={this.state.recipe.image_url}/>
          </div>
          <div className={"col-md-8"}>
            <Ingredients ingredients={this.state.recipe.ingredients}/>
          </div>
        </div>
      </>
    );
  }
}

export default RecipeItem;