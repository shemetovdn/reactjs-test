import React from "react";
import Row from "./row.jsx";
import SearchPanel from "./search-panel.jsx";
import LoaderImage from "../../img/basicloader.gif";

class RecipesList extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      recipes: [],
      count: 0,
      isLoading: true
    };
  }

  setRecipes = (recipies, count) => {
    this.setState({
      recipies: recipies,
      count: count,
      isLoading: false
    });
  }

  renderRows () {
    if (this.state.isLoading) {
      return <img className={"loader"} src={LoaderImage}/>;
    } else {
      if (this.state.recipies instanceof Object) {
        return this.state.recipies.map((item, i) => (<Row key={i} recipe={item}/>));
      }
    }
  }

  render () {
    return (
      <>
        <SearchPanel setRecipes={this.setRecipes}/>
        <table className="container">
          <thead className="container">
            <tr className="row">
              <th className="col-md-3">photo</th>
              <th className="col-md-3">title</th>
              <th className="col-md-3">publisher</th>
              <th className="col-md-2">rank</th>
              <th className="col-md-1"></th>
            </tr>
          </thead>
          <tbody className="container">
            {this.renderRows()}
          </tbody>
        </table>
      </>
    );
  }
}

export default RecipesList;