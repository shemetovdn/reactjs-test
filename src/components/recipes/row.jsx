import React from "react";
import {
  NavLink
} from "react-router-dom";

class Row extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <tr className="row">
        <td className="col-md-3">
          <img src={this.props.recipe.image_url} style={{ width: "100px" }}/>
        </td>
        <td className="col-md-3">{this.props.recipe.publisher}</td>
        <td className="col-md-3">{this.props.recipe.title}</td>
        <td className="col-md-3">{this.props.recipe.social_rank}</td>
        <td className="col-md-3"><NavLink
          to={"/recipe/" + this.props.recipe.recipe_id}>Show details</NavLink>
        </td>
      </tr>
    );
  }
}

export default Row;