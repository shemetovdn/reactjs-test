import React from "react";
import Router from "./components/common/router.jsx";
import "bootstrap/dist/css/bootstrap.min.css";
import "./app.css";

class App extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {

    return (
      <Router/>
    );
  }
}

export default App;

