import React from "react";
import ReactDOM from "react-dom";
import App from "./app.jsx";

const AppElem = document.getElementById("app");

if (AppElem) {
  ReactDOM.render(<App/>, AppElem);
}
